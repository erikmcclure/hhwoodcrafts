+++
title = "Cutting Boards"
image = "img/cutting-boards/guitar-board-2_15098047340_o.jpg"
+++

Our cutting boards bring fun and functionality to your kitchen. Our wooden boards are available in a variety of sizes and are designed to provide years of service and enjoyment. Boards can be cut to your specifications. Traditional boards are made of honey maple. However, many of our customers desire a little something different. Combination wood boards are very popular. Walnut, cherry, and domestic renewable hardwoods are available upon request. Design your board and express yourself. Think outside the rectangle.

Animal shapes, musical instruments, and themed boards are very popular We are often called upon to create and design boards to commemorate weddings, anniversaries, and friendships.

We have produced boards for corporate giving and will work with your logo upon request. Design fees may apply and quantity discounts are available.