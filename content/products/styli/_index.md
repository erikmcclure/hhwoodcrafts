+++
title = "Styli"
image = "img/styli/styli_15098171498_o.jpg"
+++

Texting just got a whole lot easier. Our fashionable and functional styluses have been designed to be used with smart phones and touch screens. Made from colorful acrylics, exotic hardwoods, and shed antler our styluses are meant to help you express yourself. Buy 2 and save.

**Mini-Stylus**

Using the same attention to design and functionality found in our popular styluses, we have decided to offer a pocket or palm size version of our popular stylus. We have proved that great things can come in small packages with this offering. Perfect for smartphones and small tablets, these minis can be tethered to your device utilizing the existing headphone jack. Available for $7.00 each or 2 for $10.00 stock up today.